﻿Imports System.ComponentModel
Imports DevExpress.XtraEditors
Imports DevExpress.XtraReports.UI

Public Class Frm_Main
    Public Sub OpenForm(Form As XtraForm, Optional Index As Boolean = False, Optional ByRef Index1 As Boolean = False)
        If Index1 = False Then Start_Waiting()
        Form.MdiParent = Me
        If Index = False Then Form.WindowState = FormWindowState.Maximized Else Form.WindowState = FormWindowState.Normal
        Form.Show()
        Form.BringToFront()
    End Sub

    Private Sub PartyBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles PartyBarButtonItem.ItemClick
        OpenForm(Frm_Party)
        End_Waiting()
    End Sub

    Private Sub InvoiceBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles InvoiceBarButtonItem.ItemClick
        If Frm_Invoice.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_Invoice.Close()
                Frm_SelectInvoiceType.StartPosition = FormStartPosition.CenterParent
                Frm_SelectInvoiceType.ControlBox = True
                Frm_SelectInvoiceType.ShowDialog()
            Else
                Frm_Invoice.Show()
                Frm_Invoice.BringToFront()
            End If
        ElseIf Frm_ServiceInvoice.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_ServiceInvoice.Close()
                Frm_SelectInvoiceType.StartPosition = FormStartPosition.CenterParent
                Frm_SelectInvoiceType.ControlBox = True
                Frm_SelectInvoiceType.ShowDialog()
            Else
                Frm_ServiceInvoice.Show()
                Frm_ServiceInvoice.BringToFront()
            End If
        Else
            Frm_SelectInvoiceType.StartPosition = FormStartPosition.CenterParent
            Frm_SelectInvoiceType.ControlBox = True
            Frm_SelectInvoiceType.ShowDialog()
        End If
        'Frm_SelectInvoiceType.StartPosition = FormStartPosition.CenterParent
        'Frm_SelectInvoiceType.ControlBox = True
        'Frm_SelectInvoiceType.ShowDialog()
    End Sub

    Private Sub InvoiceReportBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim Rpt As New XR_Invoice
        Rpt.FillDataSource()
        Dim ReportPrintTool = New ReportPrintTool(Rpt)
        End_Waiting()
        Rpt.ShowRibbonPreviewDialog()
    End Sub

    Private Sub Frm_Main_Closing(sender As Object, e As CancelEventArgs) Handles MyBase.Closing
        Application.Exit()
    End Sub

    Private Sub ExitBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ExitBarButtonItem.ItemClick
        'Application.Exit()
        Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the Program?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then
            Application.Exit()
        Else
        End If
    End Sub

    Private Sub CompanyBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles CompanyBarButtonItem.ItemClick
        If Me.MdiChildren.Length <= 0 Then
            Frm_SelectCompany.StartPosition = FormStartPosition.CenterParent
            Frm_SelectCompany.ControlBox = True
            Frm_SelectCompany.ShowDialog()
        Else
            XtraMessageBox.Show("Please Close All Opened Form(s)", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub UserBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles UserBarButtonItem.ItemClick
        OpenForm(Frm_User)
        With Frm_User
            ' .InitLookup()
        End With
        End_Waiting()
    End Sub

    Private Sub Frm_Main_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'If Not PubIsAdmin Then
        '    ClientPaymentBarButtonItem.Enabled = False
        '    VendorPaymentBarButtonItem.Enabled = False
        'End If
        OpenForm(Frm_Dashboard)
        End_Waiting()
    End Sub

    Private Sub CategoryBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles CategoryBarButtonItem.ItemClick
        OpenForm(Frm_Category)
        End_Waiting()
    End Sub

    Private Sub SubCategoryBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles SubCategoryBarButtonItem.ItemClick
        OpenForm(Frm_SubCategory)
        With Frm_SubCategory
            .InitLookup()
        End With
        End_Waiting()
    End Sub

    Private Sub ProductBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ProductBarButtonItem.ItemClick
        OpenForm(Frm_Product)
        With Frm_Product
            .InitLookup()
        End With
        End_Waiting()
    End Sub

    Private Sub CompanyProfileBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles CompanyProfileBarButtonItem.ItemClick
        OpenForm(Frm_Company)
        End_Waiting()
    End Sub

    Private Sub BankDetailsBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BankDetailsBarButtonItem.ItemClick
        OpenForm(Frm_Bank)
        End_Waiting()
    End Sub

    Private Sub ProformaInvoiceBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ProformaInvoiceBarButtonItem.ItemClick
        OpenForm(Frm_ProformaInvoice)
        With Frm_ProformaInvoice
            .InitLookup()
        End With
        End_Waiting()
    End Sub

    Private Sub VendorBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles VendorBarButtonItem.ItemClick
        OpenForm(Frm_Vendor)
        End_Waiting()
    End Sub

    Private Sub PurchaseOrderBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles PurchaseOrderBarButtonItem.ItemClick
        If Frm_PurchaseOrder.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_PurchaseOrder.Close()
                Frm_SelectPOType.StartPosition = FormStartPosition.CenterParent
                Frm_SelectPOType.ControlBox = True
                Frm_SelectPOType.ShowDialog()
            Else
                Frm_PurchaseOrder.Show()
                Frm_PurchaseOrder.BringToFront()
            End If
        Else
            Frm_SelectPOType.StartPosition = FormStartPosition.CenterParent
            Frm_SelectPOType.ControlBox = True
            Frm_SelectPOType.ShowDialog()
        End If
    End Sub

    Private Sub PurchaseProductBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles PurchaseProductBarButtonItem.ItemClick
        OpenForm(Frm_POProduct)
        With Frm_POProduct
            .InitLookup()
        End With
        End_Waiting()
    End Sub

    Private Sub WorkOrderBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles WorkOrderBarButtonItem.ItemClick
        If Frm_WorkOrder.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_WorkOrder.Close()
                Frm_SelectWOType.StartPosition = FormStartPosition.CenterParent
                Frm_SelectWOType.ControlBox = True
                Frm_SelectWOType.ShowDialog()
            Else
                Frm_WorkOrder.Show()
                Frm_WorkOrder.BringToFront()
            End If
        Else
            Frm_SelectWOType.StartPosition = FormStartPosition.CenterParent
            Frm_SelectWOType.ControlBox = True
            Frm_SelectWOType.ShowDialog()
        End If
    End Sub

    Private Sub ProInvoiceBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ProInvoiceBarButtonItem.ItemClick
        If Frm_ProformaInvoice.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_ProformaInvoice.Close()
                Frm_SelectProformaInvoiceType.StartPosition = FormStartPosition.CenterParent
                Frm_SelectProformaInvoiceType.ControlBox = True
                Frm_SelectProformaInvoiceType.ShowDialog()
            Else
                Frm_ProformaInvoice.Show()
                Frm_ProformaInvoice.BringToFront()
            End If
            'ElseIf Frm_ServiceInvoice.Visible Then
            '    Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            '    If result = DialogResult.Yes Then
            '        Frm_ServiceInvoice.Close()
            '        Frm_SelectProformaInvoiceType.StartPosition = FormStartPosition.CenterParent
            '        Frm_SelectProformaInvoiceType.ControlBox = True
            '        Frm_SelectProformaInvoiceType.ShowDialog()
            '    Else
            '        Frm_ServiceInvoice.Show()
            '    End If
        Else
            Frm_SelectProformaInvoiceType.StartPosition = FormStartPosition.CenterParent
            Frm_SelectProformaInvoiceType.ControlBox = True
            Frm_SelectProformaInvoiceType.ShowDialog()
        End If
    End Sub

    Private Sub SalesReportBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles SalesReportBarButtonItem.ItemClick
        If Frm_SalesReport.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_SalesReport.Close()
                Frm_SelectInvoiceTypeForReport.StartPosition = FormStartPosition.CenterParent
                Frm_SelectInvoiceTypeForReport.ControlBox = True
                Frm_SelectInvoiceTypeForReport.ShowDialog()
            Else
                Frm_SalesReport.Show()
                Frm_SalesReport.BringToFront()
            End If
        ElseIf Frm_ServiceReportNew.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_ServiceReportNew.Close()
                Frm_SelectInvoiceTypeForReport.StartPosition = FormStartPosition.CenterParent
                Frm_SelectInvoiceTypeForReport.ControlBox = True
                Frm_SelectInvoiceTypeForReport.ShowDialog()
            Else
                Frm_ServiceReportNew.Show()
                Frm_ServiceReportNew.BringToFront()
            End If
        Else
            Frm_SelectInvoiceTypeForReport.StartPosition = FormStartPosition.CenterParent
            Frm_SelectInvoiceTypeForReport.ControlBox = True
            Frm_SelectInvoiceTypeForReport.ShowDialog()
        End If
        'OpenForm(Frm_SelectInvoiceTypeForReport)
        'End_Waiting()
    End Sub

    Private Sub BackupBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BackupBarButtonItem.ItemClick
        Dim portfolioPath As String = My.Settings.dbPreevolTechnicsConnectionString1
        Dim length As Integer = Len(portfolioPath)
        Dim subPath As String = portfolioPath.Substring(47, length - 48)
        Try
            FileCopy(subPath, "C:\Users\bhavsar\Google Drive\Back up of Preevol DB\dbPreevolTechnics.accdb")
            XtraMessageBox.Show("Database Backup Successfully Done...", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            XtraMessageBox.Show("Please Take Backup From PC Where GOOGLE Drive is Configured.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'FileCopy(subPath, "\\BHAVSAR-PC\Users\bhavsar\Google Drive\Back up of Preevol DB\dbPreevolTechnics.accdb")
            'XtraMessageBox.Show("Database Backup Successfully Done...", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    Private Sub QuotationBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles QuotationBarButtonItem.ItemClick, BarButtonItem8.ItemClick
        If Frm_Quotation.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_Quotation.Close()
                Frm_SelectQuotationType.StartPosition = FormStartPosition.CenterParent
                Frm_SelectQuotationType.ControlBox = True
                Frm_SelectQuotationType.ShowDialog()
            Else
                Frm_Quotation.Show()
                Frm_Quotation.BringToFront()
            End If
        Else
            Frm_SelectQuotationType.StartPosition = FormStartPosition.CenterParent
            Frm_SelectQuotationType.ControlBox = True
            Frm_SelectQuotationType.ShowDialog()
        End If
    End Sub

    Private Sub TypeBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles TypeBarButtonItem.ItemClick
        OpenForm(Frm_Type)
        End_Waiting()
    End Sub

    Private Sub PlungerDiaBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles PlungerDiaBarButtonItem.ItemClick
        OpenForm(Frm_PlungerDia)
        End_Waiting()
    End Sub

    Private Sub ModelBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ModelBarButtonItem.ItemClick
        OpenForm(Frm_Model)
        End_Waiting()
    End Sub

    Private Sub GoodsBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles GoodsBarButtonItem.ItemClick
        OpenForm(Frm_Goods)
        End_Waiting()
    End Sub

    Private Sub InventoryBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles InventoryBarButtonItem.ItemClick
        If Frm_InventoryManagement.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_InventoryManagement.Close()
                Frm_SelectInventoryType.StartPosition = FormStartPosition.CenterParent
                Frm_SelectInventoryType.ControlBox = True
                Frm_SelectInventoryType.ShowDialog()
            Else
                Frm_InventoryManagement.Show()
                Frm_InventoryManagement.BringToFront()
            End If
        Else
            Frm_SelectInventoryType.StartPosition = FormStartPosition.CenterParent
            Frm_SelectInventoryType.ControlBox = True
            Frm_SelectInventoryType.ShowDialog()
        End If
    End Sub

    Private Sub DBBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles DBBarButtonItem.ItemClick
        OpenForm(Frm_Dashboard)
        End_Waiting()
    End Sub

    Private Sub StockReportBarButton_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles StockReportBarButton.ItemClick
        OpenForm(Frm_StockReport)
        End_Waiting()
    End Sub
End Class